import 'purecss/build/pure.css'

import React from 'react';
import ReactDOM from 'react-dom';
import { Route, Router, IndexRoute, hashHistory } from 'react-router';
// import App from './components/App';
// import Email from './components/Email';
import Layout from './components/layouts/Layout';
// import Blog from './components/Blog';

import About from './pages/About';
import Home from './pages/Home';
import Contact from './pages/Contact';
import Article from './pages/Article';
import Todos from './pages/Todos';

const root = document.getElementById('root');

ReactDOM.render(
    <Router history={hashHistory}>
      <Route path="/" component={Layout}>
        <IndexRoute component={Home}></IndexRoute>
        <Route path="about" name="about" component={About}></Route>
        <Route path="contact" name="contact" component={Contact}></Route>
        <Route path="article(/show/:id)" name="article" component={Article}></Route>
        <Route path="todos" name="todos" component={Todos}></Route>
      </Route>
    </Router>
  ,root);
