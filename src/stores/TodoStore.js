import { EventEmitter } from "events";

import dispatcher from "../dispatcher";

class TodoStore extends EventEmitter {
  constructor() {
    super();
    this.todos = [
      {
        id: 12345,
        text: "Go Shopping",
        complete: false
      },
      {
        id: 54321,
        text: "Pay Bills",
        complete: false
      }
    ];
  }

  createTodo(text){
    const id = Date.now();
    this.todos.push({
      id,
      text,
      complete: false,
    });

    this.emit('change');
  }

  getAll(){
    return this.todos;
  }

  handleActions(action){
    // console.log('TodoStore received an action',action);
    switch (action.type) {
      case "CREATE_TODO": {
        this.createTodo(action.text);
      }
    }
  }
}

const todoStore = new TodoStore;
dispatcher.register(todoStore.handleActions.bind(todoStore));
// window.todoStore = todoStore;
// window.dispatcher = dispatcher;
export default todoStore;
