import React, { Component } from 'react';

class Todo extends Component {
  render() {
    const { text } = this.props;
    return (
      <li>{text} <b>X</b> </li>
    )
  }
}

export default Todo;
