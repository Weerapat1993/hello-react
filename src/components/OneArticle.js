import React, { Component } from 'react';

class OneArticle extends Component {
  render() {
    const { title } = this.props;
    return (
      <div>
        <h2 className="content-subhead">{title}</h2>
        <p>
            To use this layout, you can just copy paste the HTML, along with the CSS in <a href="/css/layouts/side-menu.css" alt="Side Menu CSS">side-menu.css</a>, and the JavaScript in <a href="/js/ui.js">ui.js</a>. The JS file uses vanilla JavaScript to simply toggle an <code>active</code> class that makes the menu responsive.
        </p>
        <a href="#" className="pure-button pure-button-primary">Read More</a>
      </div>
    )
  }
}

export default OneArticle;
