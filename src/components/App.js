import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import data from './data';

const Contact = props =>
  <div className="pure-u-1-3">
    <h2>{props.name}</h2>
    <p>{props.email}</p>
  </div>;

class App extends Component {
  componentWillMount() {
    this.setState({
      contacts: data,
    });
  }
  addContact = (e) => {
    e.preventDefault()

    const contacts = this.state.contacts;
    const newId = contacts[contacts.length - 1].id + 1;

    this.setState({
      contacts: contacts.concat({
        id: newId,
        name: this.refs.name.value,
        email: this.refs.email.value,
      })
    });

    this.refs.name.value = null;
    this.refs.email.value = null;

    console.log('clicked !!');
  }

  newContact = () =>
    <div className="pure-g">
      <div className="pure-u-12-24">
        <form className="pure-form" onSubmit={this.addContact}>
          <fieldset>
            <legend>New Contact</legend>

            <input ref="email" type="email" placeholder="email@example.com"/>
            <input ref="name" type="text" placeholder="Name" />

            <button className="pure-button pure-button-primary" type="submit">Submit</button>
          </fieldset>
        </form>
      </div>
    </div>;

  render() {
    return (
      <div>
        <div className="App">
          <div className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            <h2>Welcome to React</h2>
          </div>
          <p className="App-intro">
            To get started, edit <code>src/App.js</code> and save to reload.
          </p>
        </div>
        <div id="App">
          {this.newContact()}
          <div className="pure-g">
            {this.state.contacts.map(info =>
              <Contact key={info.id} {...info} />
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
