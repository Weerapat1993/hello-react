import React, { Component } from 'react';

class Header extends Component {
  handleChange(e) {
    const title = e.target.value;
    this.props.changeTitle(title);
  }

  render() {
    return (
      <div className="header">
          <h1>{this.props.title}</h1>
          <input type="text" value={this.props.title} onChange={this.handleChange.bind(this) }/>
          <h2>A subtitle for your page goes here</h2>
      </div>
    )
  }
}

export default Header;
