import React, { Component } from 'react';
import Menu from './Menu';
import Header from './Header';

class Layout extends Component {
  constructor() {
    super();
    this.state = {
      title: "Welcome",
    };
  }

  changeTitle(title) {
    this.setState({title});
  }

  toggleClass(element, className){
    var classes = element.className.split(/\s+/),
        length = classes.length,
        i = 0;

    for(; i < length; i++) {
      if (classes[i] === className) {
        classes.splice(i, 1);
        break;
      }
    }
    // The className is not found
    if (length === classes.length) {
        classes.push(className);
    }

    element.className = classes.join(' ');
  }

  toggleAll = (e) => {
    var active = 'active';

    e.preventDefault();
    this.toggleClass(document.getElementById('layout'), active);
    this.toggleClass(document.getElementById('menu'), active);
    this.toggleClass(document.getElementById('menuLink'), active);
  }

  menuLink = (e) => {
    this.toggleAll(e);
  }

  content = (e) => {
    if (document.getElementById('menu').className.indexOf('active') !== -1) {
        this.toggleAll(e);
    }
  }

  render() {
    return (
      <div id="layout">
          <a href="#menu" id="menuLink" className="menu-link" onClick={this.menuLink}>
              <span></span>
          </a>
          <Menu />

          <div id="main">
            <Header changeTitle={this.changeTitle.bind(this)} title={this.state.title} />
          </div>
          {this.props.children}
      </div>
    )
  }
}

export default Layout;
