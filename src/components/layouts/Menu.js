import React, { Component } from 'react';

class Menu extends Component {
  render() {
    return (
      <div id="menu">
          <div className="pure-menu">
              <a className="pure-menu-heading" href="#">React</a>

              <ul className="pure-menu-list">
                  <li className="pure-menu-item"><a href="#/" className="pure-menu-link">Home</a></li>
                  <li className="pure-menu-item"><a href="#/about" className="pure-menu-link">About</a></li>
                  <li className="pure-menu-item"><a href="#/contact" className="pure-menu-link">Contact</a></li>
                  <li className="pure-menu-item"><a href="#/article" className="pure-menu-link">Article</a></li>
                  <li className="pure-menu-item"><a href="#/todos" className="pure-menu-link">Todos</a></li>
              </ul>
          </div>
      </div>
    )
  }
}

export default Menu;
