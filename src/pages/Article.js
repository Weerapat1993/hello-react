import React, { Component } from 'react';
import OneArticle from '../components/OneArticle';

class Article extends Component {
  render() {
    const Articles = [
      "Article 1",
      "Article 2",
      "Article 3",
      "Article 4",
    ].map((title, i) => <OneArticle key={i} title={title}/>);

    console.log(this.props);
    const { query } = this.props.location;
    const { params } = this.props;
    const { id } = params;
    const { date,filter } = query;
    return (
      <div className="content">
        <h2 className="content-subhead">Article Show ({id})</h2>
        <p>
            ID : {id}
        </p>
        <p>
            date : {date}, filter : {filter}
        </p>
        <div className="pure-g">
          <div className="pure-u-1-1">
            {Articles}
          </div>
        </div>
      </div>
    )
  }
}

export default Article;
