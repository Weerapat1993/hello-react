import React, { Component } from 'react';

import Todo from '../components/Todo';
import * as TodoActions from '../actions/TodoActions';
import TodoStore from '../stores/TodoStore';

class Todos extends Component {
  constructor(){
    super();
    this.state = {
      todos: TodoStore.getAll()
    }
  }

  componentWillMount(){
    TodoStore.on('change',() => {
      this.setState({
        todos: TodoStore.getAll(),
      });
    });
  }

  createTodo(){
    TodoActions.createTodo(Date.now());
  }

  render() {
    const { todos } = this.state;

    const TodoComponents = todos.map((todo) => {
       return <Todo key={todo.id} {...todo} />;
    });

    console.log(todos);

    return (
      <div className="content">
        <br/>
        <button onClick={this.createTodo.bind(this)} className="pure-button pure-button-primary">Create</button>
        <h2 className="content-subhead">Todos</h2>
        <ul>{TodoComponents}</ul>
      </div>
    )
  }
}

export default Todos;
